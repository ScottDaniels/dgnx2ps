
/*
	Abstract:	Postscript to draw a step thing. This is a chevron like thing pointing right.
				"inset" on the function call is the distance in that the edges are "pushed".

	Date:		10 Apr 2024
	Author:		E. Scott Daniels
*/

package ioxml2eps

import (
	"io"
)

var psStepWritten bool = false
var psStepCode string = `
	% set the path of the step thing
	% x y width height inset stepthing path
	/stepthing {
		/_stinset ed
		newpath
		4 2 roll moveto 			% width height
		exch						% height width
		dup							% height width width
		_stinset sub				% top line is pulled back
		0 rlineto exch dup			% width height height
		2 div _stinset exch			% width height inset h/2
		rlineto dup 2 div			% width height h/2
		_stinset neg exch			% width height -inset h/
		rlineto						% width height
		exch _stinset sub neg 0		% height -dist 0
		rlineto 2 div				% h/2
		_stinset  exch neg			% dist -h/2
		rlineto
		closepath
	} def

	% filled step thing; stroking with current colour
	% r g b x y width height inset fstepthing
	/fstepthing {
		5 copy stepthing	% dup everything and set the path
		gsave
			8 5 roll
			setrgbcolor	
			fill
		grestore
		stepthing
		stroke
	} def

	% fill only step thing (no stroke)
	% r g b x y width height inset
	/fostepthing {
		stepthing
		gsave
			setrgbcolor
			fill
		grestore
	} def
`

func psStepWrite( out io.Writer ) {
	if ! psStepWritten {
		psUtilWrite( out )	// ensure we are set up
		out.Write( squishPS( psStepCode, true ) )
		psStepWritten = true
	}
}
