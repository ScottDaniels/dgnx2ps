
/*
	Abstract:	Test the drawing things a bit.

	Date:		19 March 2022
	Author:		Scott Daniels
*/

package ioxml2eps

import (
	"fmt"
	"os"
	"testing"
)

func setupWriters( ) {
	psRptWrite( os.Stderr )		// bloody go test writes to stdout, so we go to err
	psRoundBoxWrite( os.Stderr )
	psBoxWrite( os.Stderr )
	psTangleWrite( os.Stderr )
	psCircleWrite( os.Stderr )
	psEllipseWrite( os.Stderr )
	psDiamondWrite( os.Stderr )
	psArcWrite( os.Stderr )
	psDiskWrite( os.Stderr )
	psPgramWrite( os.Stderr )
	psWriterWrite( os.Stderr )
	psDstoreWrite( os.Stderr )
	psColourWrite( os.Stderr )
}

/*
	This is a basic test that should generate postscript output that can
	be captured and rendered with Ghostscript. Ouput is to stderr so
	as to avoid any go test ouput. This works nicely for visual verification:
		go test -test.run TestDThing 2>/tmp/x.ps
		ghostscript /tmp/x.ps
*/
func TestDThing( t *testing.T ) {
	fname := "test_data/dt_test.xml"
	drawing, err := NewSketch( fname )				// laod the drawing from disk
	abortIfTrue( t, err != nil, fmt.Sprintf( "xml file %q could not be loaded: %s", fname, err ) )

	minPt, maxPt := drawing.BoundingBox()
	fmt.Fprintf( os.Stderr, "%%!PS-Adobe-3.0 EPSF-3.0\n" )
	fmt.Fprintf( os.Stderr, "%%%%BoundingBox: %s %s\n", minPt, maxPt );
	_, y := maxPt.XY()
	fmt.Fprintf( os.Stderr, "0 %d translate\n", y )	// the drawing assumes quadrant 4

	fmt.Fprintf( os.Stderr, "%% <INFO> drawing loaded from %s\n", fname )
	fmt.Fprintf( os.Stderr, "%% <INFO> %d pages %d elements\n", drawing.Pages(), drawing.EleCount() )

	setupWriters()
	//scaleX, scaleY := drawing.PageScale()
	//fmt.Fprintf( os.Stderr, "%.2f %.2f scale\n", scaleX, scaleY )

	drawing.SelectSketch( 0 )

	fmt.Fprintf( os.Stderr, "  %% =========================================================\n" )
	nEles := drawing.EleCount()
	for i := 0; i < nEles; i++ {
		ele := drawing.Element( i )
		if ele.Kind() == dtLine {
			pts := ele.Points()
			fmt.Fprintf( os.Stderr, "%% element %d is a line with %d points\n", i, len(pts) )
		} else {
			fmt.Fprintf( os.Stderr, "%% element %d is a %v\n", i, ele.Kind() )
		}
		ele.Paint( os.Stderr )
	}
	fmt.Fprintf( os.Stderr, "  %% =========================================================\n" )
	
	ele := drawing.Element( 2 )
	abortIfTrue( t, ele == nil, "couldn't find element" )
	fmt.Fprintf( os.Stderr, "%%EOF\n" )
}
