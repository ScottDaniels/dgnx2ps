
/*
	Abstract:	This describes the XML layout in terms of Go structs
				that are used as destinations when the XML is unpacked.

	Date:		12 March 2022
	Author:		E. Scott Daniels
*/

package ioxml2eps

import (
	"fmt"
//"os"
)


/*
	The mkFile layer. The only interesting thing it contains is the diagam.
	Even though there is a "page" attribute inside of the mxGraphModel 
	object, "pages" seem to be managed with separate diagram objects.
	(The XML organisation is simply mind boggling.) So, we manage an
	array of Dgram objects.
*/
type DFile struct {
	Version	string		`xml:"version,attr"`
	Dgrams	[]*Diagram	`xml:"diagram"`

	page	int			// current indoex into Dgram
}

/*
	Select the next page; return false if it cannot be selected
*/
func (df *DFile) Next(  ) bool {
	if df == nil {
		return false
	}

	if df.page + 1 == len( df.Dgrams ) {
		return false
	}

	df.page++
	return true
}

/*
	Get the next page if we can. If there isn't a next page then
	return nil.
*/
func (df *DFile) NextPage( ) *Diagram {
	if df == nil {
		return nil
	}

	if df.Next() {
		return df.Dgrams[df.page]
	}

	return nil
}

/*
	Get the currently selected page
*/
func (df *DFile) Page( ) *Diagram {
	if df == nil {
		return nil
	}

	return df.Dgrams[df.page]
}

func (df *DFile) PageCount(  ) int {
	if df == nil {
		return 0
	}
	return len( df.Dgrams )
}

/*
	Select a spdcific page
*/
func (df *DFile) Select( n int ) {
	if df == nil || n < 0 || n >= len(df.Dgrams) {
		return
	}

	df.page = n
}

// -----------------------------------------------------------------------------------------


/*
	The diagram contains one or more models which seems to be a page.
	(Though creating a new page in the tool crates a new diagram rather than
	adding a page to the diagram; sheesh!)
*/
type Diagram struct {
	Id		string		`xml:"id,attr"`
	Page	[]Model		`xml:"mxGraphModel"`
}

/*
	The model's attibutes describe the overall page which is likely bigger
	than the bounding box. The drawing ("root") and height/width are  the only 
	things that seem useful:

		dx="884" dy="695" grid="1" gridSize="10" guides="1" tooltips="1" connect="1" 
		arrows="1" fold="1" page="1" pageScale="1" pageWidth="850" pageHeight="1100" 
		math="0" shadow="0">
*/
type Model struct {
	Id		string		`xml:"id,attr"`
	PgHeight string		`xml:"pageHeight,attr"`
	PgWidth string		`xml:"pageWidth,attr"`
	Slate	Drawing		`xml:"root"`
}

/*
	Given the drawings h/w compute the x,y scales to scale the drawing to a
	US-Letter sized page
*/
func (m *Model) LetterScale() (float64, float64) {
	if m == nil {
		return 1.0 ,1.0
	}

	h := float64( atoi(m.PgHeight) )
	w := float64( atoi(m.PgWidth) )

	lw := float64( 8.5 ) * float64( 72 )
	lh := float64( 11.0 ) * float64( 72 )

	return lw/w, lh/h
}

/*
	The drawing contains multiple drawing things which it calls cells.
*/
type Drawing struct {
	Things []DThing		`xml:"mxCell"`
}

/*
	A drawing thing's geometry is described by the attributes in this portion.
*/
type Geometry struct {
	X	string		`xml:"x,attr"`
	Y	string		`xml:"y,attr"`
	Width	string	`xml:"width,attr"`
	Height	string	`xml:"height,attr"`
	Points	[]Point	`xml:"mxPoint"`// seems to define the end points only (src and target)
	InnerPts Array	`xml:"Array"`
}

type Point struct {
	X	string	`xml:"x,attr"`
	Y	string	`xml:"y,attr"`
	Kind	string `xml:"as,attr"`	// srcPoint or targetPoint
}

func (pt *Point) Set( x, y int ) {
	if pt == nil {
		return
	}

	pt.X = fmt.Sprintf( "%d", x )
	pt.Y = fmt.Sprintf( "%d", y )
}

func (pt *Point) XY( ) (x, y int ) {
	if pt == nil {
		return 0, 0
	}

	return atoi( pt.X ), atoi( pt.Y )
}

/*
	Return a real point.
*/
func (pt *Point) RealPt( ) *RealPoint {
	if pt == nil {
		return NewRealPoint( 0, 0 )
	}

	return NewRealPoint( atoi(pt.X), atoi(pt.Y) )
}

func (pt *Point) XYf64( ) (xf, yf float64 ) {
	if pt == nil {
		return 0.0, 0.0
	}

	x, y := pt.XY()
	return float64(x), float64(y)
}

/*
	In the case of a multi point line, the inner points are defined in an
	array, rather than just defining them at the geom level (sheesh).	
*/
type Array struct {
	Kind string 		`xml:"as,attr"`
	Points	[]Point		`xml:"mxPoint"`
}

/*
	Return all points from the drawing thing's geometry in a single array.
	The first point is the "source" and the last is the "target" if this
	matters to the caller.
*/
func (gm *Geometry) AllPoints() []Point {
	if gm == nil {
		return nil
	}

	pts := make( []Point, 0 )
	var pTarget Point
	if len( gm.Points ) < 1 {
		return pts
	}

	pSrc := gm.Points[0]
	if pSrc.Kind == "sourcePoint" {
		pTarget = gm.Points[1]
	} else {
		pSrc = gm.Points[1]
		pTarget = gm.Points[0]
	}

	pts = append( pts, pSrc )	// add the first point, then all intermediate ones
	for _, pt := range gm.InnerPts.Points {
		pts = append( pts, pt )
	}
	pts = append( pts, pTarget ) // finally stuff on the tail

	return pts
}

func (gm *Geometry) HeightWidth() (height, width int ) {
	if gm == nil {
		return 0, 0
	}

	return atoi(gm.Height), atoi(gm.Width)
}

func (gm *Geometry) WidthHeight() (width, height int ) {
	if gm == nil {
		return 0, 0
	}

	return atoi(gm.Width), atoi(gm.Height)
}

