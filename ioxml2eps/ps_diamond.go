/*
	Abstract:	Postscript to support drawing a diamond

	Date:		25 April 2022
	Author:		E. Scott Daniels
*/

package ioxml2eps

import (
	"io"
)


var psDiamondWritten bool = false
var psDiamondCode string = `
	% x y w h dmdPath
	/dmdPath {
		/h ed /w ed /y ed /x ed
		x w 2 div add y h add moveto
		x y h 2 div add lineto
		x w 2 div add y lineto
		x w add y h 2 div add lineto
		closepath
	} def

	% x y h w diamond 
	/diamond {
		dmdPath stroke
	} def

	% r g b x y h w fdiamond 
	/fdiamond {
		gsave
			7 4 roll
			setrgbcolor
			4 copy
			dmdPath
			fill
		grestore
		dmdPath stroke
	} def

	% fill only
	% r g b x y h w fodiamond 
	/fodiamond {
		gsave
			7 4 roll
			setrgbcolor
			dmdPath
			fill
		grestore
	} def
`

func psDiamondWrite( out io.Writer ) {
	if ! psDiamondWritten {
		psUtilWrite( out )	// ensure we are set up
		out.Write( squishPS( psDiamondCode, true ) )
		psDiamondWritten = true
	}
}
