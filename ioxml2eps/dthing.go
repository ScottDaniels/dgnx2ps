/*
	Abstract:	A DThing is a single element in the drawing. It is initialised
				via an XML unmarshal event, thus there is no New function.

				Because we don't need to manage individual types of drawing
				things, there are no "sub-structures" under the base DT
				struct. Painting is the only operation which needs to know
				and/or understand type of thing, and it is just easier to
				manage a set of methods with an "upper level" switch to
				invoke the correct one than to support the substructs.
			
				All anchor points are assumed to be upper left corner, and
				bounding box is given with that in mind. IO draw's origin
				(0,0) is upper left rather than lower left. This means that
				Y values increase as they move down the page. From a postscript
				perspective, the Y values are increasingly negative as they
				move down the page; (0,0) is at the upper left, however the
				plane is quatarant 4, not some twisted geometry that IOd uses.

	Date:		19 March 2022
	Author:		E. Scott Daniels
*/

package ioxml2eps

import (
	"fmt"
	"io"
	"math"
)

/*
	Global tracking of last things so that we reduce the number
	of "set to the same" occurances in the output.
*/
var lastWidth int = 0
var lastDash string = ""

func setLineWidth( out io.Writer, desired int ) {
	if desired != lastWidth {
		lastWidth = desired
		psFprintf( out, " %d setlinewidth\n", desired )
	}
}

func setDash( out io.Writer, desired string ) {
	if desired != lastDash {
		lastDash = desired
		psFprintf( out, "[ %s ] 0 setdash\n", desired )
	}
}

/*
	A single drawing thing.
*/
type DThing struct {
	Id			string		`xml:"id,attr"`
	Value		string		`xml:"value,attr"`
	StyleStr	string		`xml:"style,attr"`
	Source		string		`xml:"source,attr"`
	Target		string		`xml:"target,attr"`
	Geom		Geometry	`xml:"mxGeometry"`

	// non-xml related things
	style		*Style
	srcThing	*DThing		// source element it is attached to
	targetThing	*DThing		// target element it is attached to
}

// ----- Support stuff ---------------------------------------------------
/*
	Suss out the rotation value and if it is set, start the
	rotation for the thing. To rotate we must translate to the
	thing's center, and then rotate the page. After rotation
	we need to translate "back" so that filling in text etc.
	will work as expected.
*/
func (dt *DThing) startRotation( out io.Writer ) {
	s := dt.Style()
	deg := s.Rotation()
	if deg == 0 {
		return
	}
	cx, cy := dt.Centre()
	psFprintf( out, "gsave %d %d translate %d rotate\n", cx, cy, -deg )	// io.d rotates backwards
	psFprintf( out, "%d %d translate\n", -cx, -cy )
}

/*
	Suss out the rotation value and if it is set, turn off
	the rotation.
*/
func (dt *DThing) finishRotation( out io.Writer ) {
	s := dt.Style()
	deg := s.Rotation()
	if deg != 0 {
		psFprintf( out, "grestore\n" )
	}
}

/*
	Return the object's center assuming x,y is lleft.
*/
func (dt *DThing) Centre() ( x int, y int ) {
	if dt == nil {
		return 0, 0
	}

	x, y = dt.XY()
	h, w := dt.Geom.HeightWidth( )

	return x + (w/2), y - (h/2)
}

/*
	Set the desired line style, size and dashed pattern,  based on what is in style.
*/
func setLineType( out io.Writer, s *Style ) {
	if s == nil {
		return
	}

	if s.IsSetWith( skDashed, "1" ) {
		pat, there := s.Value( skDashPat )
		if there {
			setDash( out, pat )
		} else {
			setDash( out, "6 3" )
		}
	} else {
		setDash( out, "1 0" )
	}

	lWidth := "1"
	lwStr, there := s.Value( skLineWidth )
	if there {
		lWidth = lwStr
	}
	setLineWidth( out, atoi(lWidth) )
}

func resetLineType( out io.Writer, width int ) {
	setDash( out, "1 0" )
	setLineWidth( out, width )
}

// -----------------------------------------------------------------------

/*
	Set the reference for src and/or target drawing things.	
*/
func (dt *DThing) AddAttachments( src, target *DThing ) {
	if dt == nil {
		return
	}

	dt.srcThing = src
	dt.targetThing = target
}

/*
	Given a "percentage" for x and y compute a real x,y pair based on this
	drawing thing's height and width. If the drawing thing is rotated
	the computed point must then be rotated round the center of the
	drawing thing. We cannot depend on postscript rotation for this.
*/
func (dt *DThing) AttachPoint( pX, pY float64 ) (x, y int) {
	if dt == nil {
		return -1, -1
	}

	if dt.style == nil {
		dt.style = parseStyle( dt.StyleStr, true, "=" )	// unpack the style junk into something usable
	}

	h, w := dt.Geom.HeightWidth()

	ox, oy := dt.XY()
	//fmt.Fprintf( os.Stderr, "%% >>>>>  attachment to %s px,y=%.2f,%.2f w,h=%d,%d x,y=%d,%d  result:", dt.Id, pX, pY, w, h, x, y )
	x =  ox + int(float64( w ) * pX)
	y =  oy - int(float64( h ) * pY)

	s := dt.Style()
	deg := s.Rotation()
	if deg != 0 {
		apt := NewRealPoint( x, y )	// attach point as a real point
		cX := ox + (w/2)			// the center for rotation
		cY := oy - (h/2)
		cpt := NewRealPoint( cX, cY )
		x, y = apt.Rotate( cpt, float64(-deg) ).XY()	// compute the rotated point and pull the coords to return
	}
	return x, y * -1
}

/*
	Return the source and target drawing thing IDs that are referenced.
*/
func (dt *DThing) Attachments() (src, target string) {
	if dt == nil {
		return
	}

	return dt.Source, dt.Target
}

/*
	Return the bounding box as two real points: min(x,y) and max(x,y)
*/
func (dt *DThing) BoundingBox() (minPt *RealPoint, maxPt *RealPoint) {
	if dt == nil {
		return nil, nil
	}

	if dt.Kind() == dtLine {
		pts := dt.RealPoints()
		return BoundingBox( pts )
	}

	x, y := dt.XY()
	y *= -1					// we need positive values not the negitive plotting y
	w, h := dt.Dimensions()
	
	minPt = NewRealPoint( x, y )
	maxPt = NewRealPoint( x + w, y + h )
	return minPt, maxPt
}

/*
	Return the width, height of the element.
*/
func (dt *DThing) Dimensions() (w int, h int) {
	if dt == nil {
		return 0, 0
	}

	h, w = dt.Geom.HeightWidth( )

	return w, h
}

/*
	Return the kind of thing.
*/
func (dt *DThing) Kind() DthingKind {
	if dt == nil {
		return dtUnknown
	}

	if dt.style == nil {
		dt.style = parseStyle( dt.StyleStr, true, "=" )	// unpack the style junk into something usable
	}

	return dt.style.DtKind()
} 

/*
	Return the drawing thing's points.
	Youi'd think that points for a DThing would be easy, but io.D can't seem to set the source and
	target points correctly if the thing is "attached" to another thing. While mostly lines,
	we need to see of there is a source or targed defined in the thing's style and if there is we
	need to compute the point(s) and sub them into the array.
*/
func (dt *DThing) Points() []Point {
	if dt == nil {
		return nil
	}

	rawPts :=  dt.Geom.AllPoints()	// pt[0] is guarenteed to be src
	//fmt.Fprintf( os.Stderr, "%% >>>> points  s=%p t=%p points: %s n=%d\n",  dt.srcThing, dt.targetThing, dt.Id, len( rawPts )  )
	if dt.srcThing != nil {
		pX := 0.0
		pY := 0.0
		val, isThere := dt.style.Value( skExitX )
		if isThere {
			pX = atof( val )
		}
		val, isThere = dt.style.Value( skExitY )
		if isThere {
			pY = atof( val )
		}
		x, y := dt.srcThing.AttachPoint( pX, pY )	// convert attach pt (a percentage) to real x,y
		//fmt.Fprintf( os.Stdout, "%% >>> +++ points: there is a src object pX,Y=(%.2f,%.2f) xy=(%d,%d)\n", pX, pY, x, y )
		rawPts[0].Set( x, y )
	}

	if dt.targetThing != nil {
		pX := 0.0
		pY := 0.0
		val, isThere := dt.style.Value( skEntryX )
		if isThere {
			pX = atof( val )
		}
		val, isThere = dt.style.Value( skEntryY )
		if isThere {
			pY = atof( val )
		}
		x, y := dt.targetThing.AttachPoint( pX, pY )	// convert attach pt (a percentage) to real x,y

		//fmt.Fprintf( os.Stdout, "%% >>> +++ points: there is a target object pX,Y=(%.2f,%.2f) xy=(%d,%d)\n", pX, pY, x, y )
		n := len( rawPts ) -1
		rawPts[n].Set( x, y )
	}

	return rawPts
}

func (dt *DThing) RealPoints() []*RealPoint {
	pts := dt.Points()
	rPts := make( []*RealPoint, 0 )
	for _, p := range( pts ) {
		rPts = append( rPts, p.RealPt() )
	}

	return rPts
}

/*
	Return the x,y coordinate of the Drawing Thing.

	postscript 0,0 is lowerleft while io.draw is upper left; we tranlate meaning 
	y values become negative
*/
func (dt *DThing) XY() (int, int) {
	if dt == nil {
		return 0, 0
	}

	x := atoi( dt.Geom.X )
	y := atoi( dt.Geom.Y )

	return x, -y
}

func (dt *DThing) Style() *Style {
	if dt == nil {
		return nil
	}

	if dt.style == nil {
		dt.style =  parseStyle( dt.StyleStr, true, "=" )
	}

	return dt.style
}

/*
	A drawing thing can paint itself, but it does require a certain amount of
	"soul searching" in order to discover what kind of thing that it is.
	To that end, and because there really isn't the need to manage specifically
	typed drawing things (e.g. dt_pgram) there are just a bunch of single
	function paint methods hung off of the DThing object.
*/
func (dt *DThing) Paint( out io.Writer) {
	if dt == nil {
		return
	}

	if dt.style == nil {
		dt.style = parseStyle( dt.StyleStr, true, "=" )	// unpack the style junk into something usable
	}

	setLineType( out, dt.style )	// set the outline type for the thing
	psWriterWrite( out )			// ensure text writing is initialised (needed by most)

	//fmt.Fprintf( os.Stderr, "%% painting: %s\n", dt.Id )
	psFprintf( out, "newpath\n" )
	switch dt.Kind( ) {	// search the soul and invoke the method that knows how to paint us
		case dtCallOut:				// commic style text bubble
			psCallOutWrite( out )
			dt.PaintCallOut( out )

		case dtCircle:
			psEllipseWrite( out )
			psCircleWrite( out )
			psArcWrite( out )
			dt.PaintCircle( out )

		case dtCloud:
			psCloudWrite( out )
			psEllipseWrite( out )
			psCircleWrite( out )
			dt.PaintCloud( out )

		case dtCylinder:
			psEllipseWrite( out )
			psCircleWrite( out )
			psArcWrite( out )
			psDiskWrite( out )
			dt.PaintCylinder( out )

		case dtDashedLine:
			psEllipseWrite( out )	// needed for circle endpoint
			psTangleWrite( out )
			dt.PaintDashedLine( out )

		case dtFlexArrow:
			dt.PaintFlexArrow( out )

		case dtHexagon:
			psHexWrite( out )
			dt.PaintHex( out )

		case dtLine:
			psEllipseWrite( out )	// needed for circle endpoint
			psTangleWrite( out )	// for arrrows
			dt.PaintLine( out )

		case dtOval:
			psArcWrite( out )
			psEllipseWrite( out )
			psCircleWrite( out )
			dt.PaintCircle( out )		// oval is just a circle with h != w constraints

		case dtParGram:
			psPgramWrite( out )
			dt.PaintPGram( out )

		case dtRect:
			psRoundBoxWrite( out )
			psBoxWrite( out )
			dt.PaintRect( out )

		case dtReport:
			psArcWrite( out )
			psRptWrite( out )
			dt.PaintReport( out )

		case dtRhombus:
			psDiamondWrite( out )
			dt.PaintRhomb( out )

		case dtRoundRect:
			psArcWrite( out )
			psRoundBoxWrite( out )
			dt.PaintRoundRect( out )

		case dtStep:
			psStepWrite( out )
			dt.PaintStep( out )

		case dtStorage:
			psDstoreWrite( out )
			dt.PaintStorage( out )

		case dtText:
			dt.PaintText( out )

		case dtTrap:
			psTrapWrite( out )
			dt.PaintTrap( out )

		case dtTangle:
			psTangleWrite( out )
			dt.PaintTangle( out )

		default:
			// silently skip
	}
}

/*
	These methods spit out postscript which draws the thing. The postscript
	assumes that the needed "canned" functions have been output first.
*/

/*
	A callout is a commic style "thought" bubble
		_______  ......
        |      |     ^
        |      |     h
		|___  _|     V
           |/ .........

	Stype parameters are obtuse in the xml:
		position:	The percentage of the width of the left side of the "gap".
		position2: the X value as a percentaege of the width of the tip
					of the decender.
		base:		The length of the gap
		size:		The distance between the box's bottom and the tip of
					the decender.

	The postscript code wants x, y, width, height, x2,y2, x3, y3, x4,y4
	where x2,y2 is the point to the left of the gap, x3,y3 is the point of
	the decender, x4,y4 is the point of the right side of the gap.

	To postscript, height is the height of the box portion, not the overall
	thing.
*/
func (dt *DThing) PaintCallOut( out io.Writer ) {
	if dt == nil {
		return
	}
	x, y := dt.XY()				// assumed to be upper left
	w, h := dt.Dimensions() 	// w assumed to be the horizonal line length + size
	
	lColour := dt.style.LineColour()
	fColour := dt.style.FillColour()
	inset := dt.style.FloatValue( skPosition2, 0.5 )	// position of decender tip
	gapStart := dt.style.FloatValue( skPosition, 0.5 )	// position of the left side of the gap
	base := dt.style.IntValue( skBase, 20 )
	size := dt.style.IntValue( skSize, 20 )				// decender distance (y)

	boxHeight := h - size							// actual height of the box
	gapSX := x + int( float32(w) * gapStart )		// x for start of gap
	gapY := y - boxHeight							// y for gap
	gapEX := gapSX + base 								// x for end of gap

	decX := x + int( float32(w) * inset )	// x for decender
	decY := y - h

	dt.startRotation( out )
	psFprintf( out, "%s setrgbcolor %s %d %d %d %d %d %d %d %d %d %d fcallout\n", 
		lColour, fColour, x, y, w, h - size, gapSX, gapY, decX, decY, gapEX, gapY )

	if dt.Value != "" {		// value has the text added to the thing
		//psFprintf( out, "%d %d mts\n", x, y + (h/2) )
		psFprintf( out, "%d %d mts\n", x, y - ((h-size)/2) )
		value2Eps( out, dt.Value, dt.style, atoi(dt.Geom.Width), atoi(dt.Geom.Height) )
	}
	dt.finishRotation( out )
}

/*
	The drawing thing's x,y is the upper left corner and the radius is computed
	by the boundng box. We can use this to draw ellipses/ovals as the bounding
	box constrains the circle properly.
*/
func (dt *DThing) PaintCircle( out io.Writer ) {
	x, y := dt.XY()
	w, h := dt.Dimensions()

	tx := x		// text x value
	x += w/2	// x,y now the center
	y -= h/2

	lColour := dt.style.LineColour()
	fColour := dt.style.FillColour()

	dt.startRotation( out )	// seems odd to rotate a circle, but this rotates text inside if it's there
	psFprintf( out, "%s setrgbcolor %s %d %d %d %d fellipse\n", lColour, fColour, x, y, w, h )
	if dt.Value != "" {
		psFprintf( out, "%d %d mts\n", tx, y )
		value2Eps( out, dt.Value, dt.style, atoi(dt.Geom.Width ), atoi(dt.Geom.Height) )
	}
	dt.finishRotation( out )
}

/*
	Painting a cloud. x,y is upper left corner.
*/
func (dt *DThing) PaintCloud( out io.Writer ) {
	x, y := dt.XY()
	w, h := dt.Dimensions()

	lColour := dt.style.LineColour()
	fColour := dt.style.FillColour()

	dt.startRotation( out )	// seems odd to rotate a circle, but this rotates text inside if it's there
	psFprintf( out, "%s setrgbcolor %s %d %d %d %d cloud\n", lColour, fColour, x, y, w, h )
	if dt.Value != "" {
		y -= h/2
		psFprintf( out, "%d %d mts\n", x, y )
		value2Eps( out, dt.Value, dt.style, atoi(dt.Geom.Width ), atoi(dt.Geom.Height) )
	}
	dt.finishRotation( out )
}

/*
	x,y of the cyl is upper left.
*/
func (dt *DThing) PaintCylinder( out io.Writer ) {
	x, y := dt.XY()
	w, h := dt.Dimensions()

	ty := y - h/2
	y -= h	// postscript x,y is lower left

	lColour := dt.style.LineColour()
	fColour := dt.style.FillColour()

	dt.startRotation( out )
	psFprintf( out, "%s setrgbcolor %s %d %d %d %d disk\n", lColour, fColour, x, y, w, h )
	if dt.Value != "" {
		psFprintf( out, "%d %d mts\n", x, ty )
		value2Eps( out, dt.Value, dt.style, atoi(dt.Geom.Width ), atoi(dt.Geom.Height) )
	}
	dt.finishRotation( out )
}

/*
	There is no need to handle dashed line differently.
*/
func (dt *DThing) PaintDashedLine( out io.Writer ) {
	dt.PaintLine( out )
}

func (dt *DThing) PaintFlexArrow( out io.Writer ) {
}

/*
	Given two points, compute and return the "angle". The angle is relative to
	the first point (ptA) given. 0 is "North" and angles increase
	anti-clockwise from thre.
*/
func lineAngle( ptA *Point, ptB *Point ) float64 {
	angle := 90.0
	ax, ay := ptA.XYf64()
	bx, by := ptB.XYf64()

	if ay != by  {			// not horizontal
		if ax == bx {		// vertical
			if by < ay {
				angle = 0
			} else {
				angle = 180
			}
		} else {
			if ay > by {
				angle = 57.2958 * math.Atan( (ax - bx) / (ay - by) )
			} else {
				angle = 180 + (57.2958 * math.Atan( (ax - bx) / (ay - by) ))
			}
		}
	} else {	// horizontal
		if bx > ax {
			angle = -90
		}
	}

	return angle
}

/*
	Paint the line and any associated arrows.
	Lines are complicated. Fiddling with the arrows, and the direction they need to point
	is only half of it. Seems that bleeding io.draw can't set source/target points correctly
	if the line is "attached" to an object. The "attachment point must be computed as a
	relationship to the attached object (because, I suppose, it is too tough to add the
	correct point coordinates when they write the XML). 

	If the line width is > 1 and there is an arrow, the end of the line must be pulled
	back to the edge of the arrow to prevent it from "showing" as the arrow comes to a point.

	The diagram.io tool's representation of where text is placed for a line is just backwards.
	"top" puts the text under the line and left justified seems to move the text to the right.
	We do our own thing that limits the damage.
*/
func (dt *DThing) PaintLine( out io.Writer ) {
	lColour := dt.style.LineColour()		// both line and fill colour

	pts := dt.Points()		// pt[0] is guarenteed to be the src
	if len( pts ) < 2 {
		return
	}

	lWidth := 1
	lwStr, there := dt.style.Value( skLineWidth )
	if there {
		lWidth = atoi( lwStr )
	}
	aWidth := (lWidth+1) * 2	// default arrow sizes for normal arrow
	aLength :=  aWidth * 2

	sArrow, tArrow := dt.style.ArrowLoc()	// arrow(s) loc and kinds
	saKind, taKind := dt.style.ArrowKind()
	var sCtrPt *RealPoint					// src/target arrow centre points if needed later
	var tCtrPt *RealPoint

	pt := pts[0].RealPt()
	apt := pts[1].RealPt()
	sAdjX, sAdjY := pt.XY()			// default to no arrow, and thus adjustment is just the point

	switch saKind {					// some arrows need "pull back"
		case arrowHalf:
			adjPt := pt.PullBack( apt, aLength + lWidth - 1 )
			sAdjX, sAdjY = adjPt.XY()

		case arrowRegular:
			adjPt := pt.PullBack( apt, aLength )
			sAdjX, sAdjY = adjPt.XY()

		case arrowOval:
			adjPt := pt.PullBack( apt, aWidth/2 )	// for a circle backoff is smaller
			sAdjX, sAdjY = adjPt.XY()
			sCtrPt = pt.PullBack( apt, aWidth/2 )

		case arrowNone:

		default:
			//fmt.Fprintf( out, "\n %% >>>> sakind: unknown type: %d\n", saKind )
	}
	setLineWidth( out, lWidth )
	fmt.Fprintf( out, "%s setrgbcolor %d %d mts ", lColour, sAdjX, -1 * sAdjY )

	cVal, cThere := dt.style.Value( skCurved )
	if cThere && cVal != "0" && len( pts ) >= 4 {	// if curve, use pts 0,1, n-2, n-1 to draw a b-curve
		nPts := len( pts )
		psFprintf( out, "%d %d %d %d ", 
			atoi( pts[1].X), -1 * atoi(pts[1].Y), atoi( pts[nPts-2].X), -1 * atoi(pts[nPts-2].Y))
	} else {
		for i := 1; i < len(pts)-1; i++ {		// segments 2 through n-1
			psFprintf( out, "%s %d lineto ", pts[i].X, -1 * atoi(pts[i].Y)  )	// all intermediate segments
		}
	}

	i := len(pts) - 1
	pt = pts[i].RealPt()			// last segment with backoff
	tAdjX, tAdjY := pt.XY()			// default to no arrow
	apt = pts[i-1].RealPt()

	switch taKind {
		case arrowHalf:
			adjPt := pt.PullBack( apt, aLength + lWidth - 1 )
			tAdjX, tAdjY = adjPt.XY()					// line end prior to arrow

		case arrowRegular:
			adjPt := pt.PullBack( apt, aLength )
			tAdjX, tAdjY = adjPt.XY()

		case arrowOval:
			adjPt := pt.PullBack( apt, aWidth/2 )	// for a circle backoff is smaller
			tAdjX, tAdjY = adjPt.XY()
			tCtrPt = pt.PullBack( apt, aLength/2 )

		case arrowNone:

		default:
			//fmt.Fprintf( out, "\n %% >>>> takind: unknown type: %d\n", saKind )
	}
	if cThere && cVal != "0" && len( pts ) >= 4 {
		psFprintf( out, "%d %d curveto ", tAdjX, -1 * tAdjY )
	} else {
		psFprintf( out, "%d %d lineto ", tAdjX, -1 * tAdjY )
	}

	psFprintf( out, "stroke\n" )
	//fmt.Fprintf( out, "%% line >>> arrows s=%v e=%v----------------------\n", sArrow, tArrow )
	
	n := len( pts )
	srcPt := pts[0]			// assume src is [0], target is [n-1]
	targetPt := pts[n-1]
	srcPrePt := pts[1]		// for multi point line, the angle of the arrow is the last segment
	targetPrePt := pts[n-2]	// these are the "previous" point for that computattion

	sx := float64( atoi( srcPt.X ) )
	sy := float64( atoi( srcPt.Y ) )
	tx := float64( atoi( targetPt.X ) )
	ty := float64( atoi( targetPt.Y ) )

	resetLineType( out, 1 )
	if sArrow {			// arrow on line at the "source"
		fill := lColour
		if ! dt.style.IsSetWith( skStartFill, "1" ) {
			fill = "1 1 1"
		}
		angle := lineAngle( &srcPrePt, &srcPt )
		switch saKind {
			case arrowHalf:
				resetLineType( out, lWidth )
				pt := NewRealPoint( int(sAdjX), int(sAdjY) )	
				nPt := pt.SlideOver( -aWidth/2, angle )
				spX, spY := nPt.XY()
				psFprintf( out, "%.0f %.0f moveto %d %d lineto %d %d lineto closepath gsave %s setrgbcolor fill grestore stroke\n",
					tx, -ty, spX, -spY, sAdjX, -sAdjY, fill )

			case arrowRegular:
				psFprintf( out, "%s %d %d %d %d %d rftangle\n", fill, int(sx), int(-sy), aLength, aWidth, int( angle ) )

			case arrowOval:
				x, y := sCtrPt.XY()
				psFprintf( out, "%s %d %d %d %d fellipse\n", fill, int(x), int(-y), aWidth, aWidth )

			default:
				fmt.Fprintf( out, "%%  ### ERR ###  (line) unrecognised start arrow kind %v\n", saKind )
		}

	}

	if tArrow {			// arrow on the line at the "target" point
		fill := lColour
		if  dt.style.IsSetWith( skEndFill, "0" ) {	// default in io.draw seems to be filled when omitted
			fill = "1 1 1"							// so "hollow" only if explicitly set to off
		}
		angle := lineAngle( &targetPrePt, &targetPt )
		switch taKind {
			case arrowHalf:
				resetLineType( out, lWidth )
				pt := NewRealPoint( int(tAdjX), int(tAdjY) )	
				nPt := pt.SlideOver( -aWidth/2, angle )
				spX, spY := nPt.XY()
				psFprintf( out, "%.0f %.0f moveto %d %d lineto %d %d lineto closepath gsave %s setrgbcolor fill grestore stroke\n",
					tx, -ty, spX, -spY, tAdjX, -tAdjY, fill )

			case arrowRegular:
				psFprintf( out, "%s %d %d %d %d %d rftangle\n", fill, int(tx), int(-ty), aLength, aWidth, int( angle ) )

			case arrowOval:
				x, y := tCtrPt.XY()
				psFprintf( out, "%s %d %d %d %d fellipse\n", fill, int(x), int(-y), aWidth, aWidth )

			default:
				fmt.Fprintf( out, "%% ### ERR ###  (line) unrecognised end arrow kind %v \n", taKind )
		}
	}
	resetLineType( out, 1 )

	if dt.Value != "" {
		mnPt, mxPt := dt.BoundingBox()
		mxX, mxY := mxPt.XY()
		mnX, mnY := mnPt.XY()

		w := mxX - mnX
		h := mxY - mnY
		x := mnX
		y := -mnY - h/2
		psFprintf( out, "%d %d mts\n", x, y )
		value2Eps( out, dt.Value, dt.style, w, h )
	}
}

/*
	Paint a hexagon; size in style is the "outset" of each 
	right/left point. x,y is the upper left corner.
*/
func (dt *DThing) PaintHex( out io.Writer ) {
	if dt == nil {
		return
	}
	x, y := dt.XY()			
	w, h := dt.Dimensions() // w assumed to be the horizonal line length + size

	y -= h	// postscript x,y is lower left

	lColour := dt.style.LineColour()
	fColour := dt.style.FillColour()
	size := dt.style.Size()		// the outset of the right/left points
	if size < 0 {
		size = 20;				// default
	}

	dt.startRotation( out )
	psFprintf( out, "%s setrgbcolor %s %d %d %d %d %d fhexagon\n", lColour, fColour, x, y, w, h, size )

	if dt.Value != "" {	// value has any text in the thing
		psFprintf( out, "%d %d mts\n", x, y + (h/2) )
		value2Eps( out, dt.Value, dt.style, atoi(dt.Geom.Width ), atoi(dt.Geom.Height) )
	}
	dt.finishRotation( out )
}

func (dt *DThing) PaintPGram( out io.Writer ) {
	if dt == nil {
		return
	}
	x, y := dt.XY()
	w, h := dt.Dimensions()

	y -= h	// postscript x,y is lower left

	lColour := dt.style.LineColour()
	fColour := dt.style.FillColour()

	dt.startRotation( out )
	psFprintf( out, "%s setrgbcolor %s %d %d %d %d 20 fpgram\n", lColour, fColour, x, y, w, h )

	if dt.Value != "" {
		psFprintf( out, "%d %d mts\n", x, y + (h/2) )
		value2Eps( out, dt.Value, dt.style, atoi(dt.Geom.Width ), atoi(dt.Geom.Height) )
	}
	dt.finishRotation( out )
}

/*
	The user interface of diagrams.net calls it a diamond, but internally
	it is labeled a rhombus.
	The x,y point is upper left corner of the bounding box.
*/
func (dt *DThing) PaintRhomb( out io.Writer ) {
	x, y := dt.XY()
	w, h := dt.Dimensions()

	tx := x		// text x pos
	y -= h		// postscript x,y is lower left

	lColour := dt.style.LineColour()
	fColour := dt.style.FillColour()

	//fmt.Fprintf( out, "%% rhombus >>>>>> x=%d y=%d  h=%d w=%d\n", x, y, h, w )
	dt.startRotation( out )
	psFprintf( out, "%s setrgbcolor %s %d %d %d %d fdiamond\n", lColour, fColour, x, y, w, h )
	if dt.Value != "" {
		psFprintf( out, "%d %d mts\n", tx, y + (h/2) )
		value2Eps( out, dt.Value, dt.style, atoi(dt.Geom.Width ), atoi(dt.Geom.Height) )
	}
	dt.finishRotation( out )
}

/*
	x,y is upper left corner
*/
func (dt *DThing) PaintRect( out io.Writer ) {
	x, y := dt.XY()
	w, h := dt.Dimensions()

	y -= h	// postscript x,y is lower left

	lColour := dt.style.LineColour()
	fColour := dt.style.FillColour()

	dt.startRotation( out )
	psFprintf( out, "%s setrgbcolor %s %d %d %d %d fbox\n", lColour, fColour, x, y, w, h )
	if dt.Value != "" {
		psFprintf( out, "%d %d mts\n", x, y + (h/2) )
		value2Eps( out, dt.Value, dt.style, atoi(dt.Geom.Width ), atoi(dt.Geom.Height) )
	}
	dt.finishRotation( out )
}

/*
	x,y is upper left
*/
func (dt *DThing) PaintRoundRect( out io.Writer ) {
	x, y := dt.XY()
	w, h := dt.Dimensions()

	y -= h	// postscript x,y is lower left

	lColour := dt.style.LineColour()
	fColour := dt.style.FillColour()

	dt.startRotation( out )
	psFprintf( out, "%s setrgbcolor %s %d %d %d %d frbox\n", lColour, fColour, x, y, w, h )
	if dt.Value != "" {
		// future: need to adjust for valign center
		psFprintf( out, "%d %d mts\n", x, y + (h/2) )
		value2Eps( out, dt.Value, dt.style, atoi(dt.Geom.Width ), atoi(dt.Geom.Height) )
	}
	dt.finishRotation( out )
}

/*
	A step (process step?)  is a chevron looking box:
		_______
        \      \
		/______/

	The "size" style parameter, if set, defines the 'indention' amount. If not set
	(why why why would it not be set???) we have to pick some magic default. When
	it is set there seem to be no units, so again we have to guess.
*/
func (dt *DThing) PaintStep( out io.Writer ) {
	if dt == nil {
		return
	}
	x, y := dt.XY()			
	w, h := dt.Dimensions() // w assumed to be the horizonal line length + size

	y -= h	// postscript x,y is lower left

	lColour := dt.style.LineColour()
	fColour := dt.style.FillColour()
	size := dt.style.Size()
	if size < 0 {
		size = 20
	}
	

	dt.startRotation( out )
	psFprintf( out, "%s setrgbcolor %s %d %d %d %d %d fstepthing\n", lColour, fColour, x, y, w, h, size )

	if dt.Value != "" {
		psFprintf( out, "%d %d mts\n", x, y + (h/2) )
		value2Eps( out, dt.Value, dt.style, atoi(dt.Geom.Width ), atoi(dt.Geom.Height) )
	}
	dt.finishRotation( out )
}

/*
	Paint a "storage" thing
*/
func (dt *DThing) PaintStorage( out io.Writer ) {
	x, y := dt.XY()
	w, h := dt.Dimensions()

	y -= h	// postscript x,y is lower left

	lColour := dt.style.LineColour()
	fColour := dt.style.FillColour()

	dt.startRotation( out )
	psFprintf( out, "%s setrgbcolor %s %d %d %d %d dstore\n", lColour, fColour, x, y, w, h )
	if dt.Value != "" {
		psFprintf( out, "%d %d mts\n", x-20, y + (h/2) ) // must adjust x because of "buldges"
		value2Eps( out, dt.Value, dt.style, atoi(dt.Geom.Width ), atoi(dt.Geom.Height) )
	}
	dt.finishRotation( out )
}

/*
	Paint a "report/ouput" thing
*/
func (dt *DThing) PaintReport( out io.Writer ) {
	x, y := dt.XY()
	w, h := dt.Dimensions()

	y -= h	// postscript x,y is lower left

	lColour := dt.style.LineColour()
	fColour := dt.style.FillColour()

	dt.startRotation( out )
	psFprintf( out, "%s setrgbcolor %s %d %d %d %d frpt\n", lColour, fColour, x, y, w, h )
	psFprintf( out, "%d %d %d %d rpt\n", x, y, w, h )
	if dt.Value != "" {
		psFprintf( out, "%d %d mts\n", x, y + (h/2) )
		value2Eps( out, dt.Value, dt.style, atoi(dt.Geom.Width ), atoi(dt.Geom.Height) )
	}
	dt.finishRotation( out )
}

/*
	Paint a triangle (always right facing). The tangle functions
	print the "point" at x,y so we must adjust x by the width
	and rotate accordingly.
*/
func (dt *DThing) PaintTangle( out io.Writer ) {
	x, y := dt.XY()
	w, h := dt.Dimensions()

	lColour := dt.style.LineColour()
	fColour := dt.style.FillColour()

	dt.startRotation( out )
	//fmt.Fprintf( out, "%% >>>>> %s setrgbcolor %s %d %d %d %d 270 rftangle\n", lColour, fColour, x+w, y - (h/2), w, h )
	psFprintf( out, "%s setrgbcolor %s %d %d %d %d 270 rftangle", lColour, fColour, x+w, y - (h/2), w, h )
	if dt.Value != "" {
		y += h/2
		psFprintf( out, "%d %d mts\n", x, y) // must adjust because x,y was on right
		value2Eps( out, dt.Value, dt.style, atoi(dt.Geom.Width ), atoi(dt.Geom.Height) )
	}
	dt.finishRotation( out )
}

/*
	Paint a text thing.
*/
func (dt *DThing) PaintText( out io.Writer ) {
	x, y := dt.XY()
	w, h := dt.Dimensions()

	dt.startRotation( out )
	if dt.Value != "" {
		psFprintf( out, "%d %d mts\n", x, y - (h/2) )
		value2Eps( out, dt.Value, dt.style, w, y )
	}
	dt.finishRotation( out )
}


/*
	Paint a trapezoid.
*/
func (dt *DThing) PaintTrap( out io.Writer ) {
	if dt == nil {
		return
	}
	x, y := dt.XY()
	w, h := dt.Dimensions() // w assumed to be the horizonal line length + size
	y += h		// x,y is lower left, but postscript functions are all upper left oriented

	y -= h	// postscript x,y is lower left

	lColour := dt.style.LineColour()
	fColour := dt.style.FillColour()
	size := dt.style.Size()		// the outset of the right/left points
	if size < 0 {
		size = 20;				// default
	}

	dt.startRotation( out )
	psFprintf( out, "%s setrgbcolor %s %d %d %d %d %d ftrapizoid\n", lColour, fColour, x, y, w, h, size )

	if dt.Value != "" {	// value has any text in the thing
		psFprintf( out, "%d %d mts\n", x, y - (h/2) )
		value2Eps( out, dt.Value, dt.style, atoi(dt.Geom.Width ), atoi(dt.Geom.Height) )
	}
	dt.finishRotation( out )
}
