/*
	Abstract:	Postscript to support drawing arcs

	Date:		25 April 2022
	Author:		E. Scott Daniels
*/

package ioxml2eps

import (
	"io"
)


var psArcWritten bool = false
var psArcCode string = `
	% arc path
	% x y width height  start-ang end-ang
	/arcPath {
			/_e ed /_s ed /_h ed /_w ed
			translate
			1 _h _w div scale
			0 0 _w 2 div _s _e   
			arc
	} def
`

func psArcWrite( out io.Writer ) {
	if ! psArcWritten {
		psUtilWrite( out )	// ensure we are set up
		out.Write( squishPS( psArcCode, true ) )
		psArcWritten = true
	}
}
