/*
# --------------------------------------------------------------------------
#	Abstract:	A no frills colour library which supports only #rrggbb and 0xrrggbb
#				mappings. ...Until we realise that bloody io.Draw adds the
#				completely borked 'rgb(r , g , b)' to the style instead of the
#				accepted #rrggbb or 0xrrggbb.  And now it seems that CSS has
#				reared its ugly head and light-dark( #rrggbb, #rrggbb ) can now
#				be used where any colour value can occurr.  The first value
#				is the value for "light" mode and the second for "dark" mode.
#
#	Date:		12 March 2022
#	Author:		E. Scott Daniels
#
# --------------------------------------------------------------------------
*/

package ioxml2eps

import (
	"fmt"
	"strings"
)

var invertColours = false		// set with InvertColours()

type Colour struct {
	str   string // the name or 0x string which was used to create it
	cval  int    // composite value
	red   int    // individual values
	green int
	blue  int
}

// ------------------------------------------------------------------------------------------

/*
	Call to set the invert colour state.
*/
func InvertColours( state bool ) {
	invertColours = state
}

/*
	Call to get inverted colour state
*/
func IsColourInverted( ) bool {
	return invertColours
}

/*
	String2colour will convert a string into a colour value. String may be one of:
		0xrrggbb
		xrrggbb
		#rrggbb

	or the not so traditional "string": rgb(r,g,b). It also seems that line colour
	is set to "none" to indicate that no line is to be drawn; it would seem to
	make more sense to set line width to 0 or somesuch.
*/
func string2colour(str string) int {
	if str == "" {
		str = "0x909090"
	}

	if str[0:2] == "0x" {
		return atoi(str)
	} else {
		if str[0:1] == "x" {
			return atoi("0" + str)
		} else {
			if str[0:1] == "#" {
				//fmt.Fprintf( os.Stderr, "%% >>>> colour set based on %s to: %d\n", str, atoi("0x" + str[1:]) )
				return atoi("0x" + str[1:])
			}
		}
	}

	if str == "none" { // if it's none, why include it in the style?
		return 0xffffff
	}

	// at this point assume we have the bloody awful representation of rgb(...) or light-dark(...)
	if str[0:4] == "rgb" {
		str = strings.Trim( str, " " )
		str = str[4:]
		l := len( str )
		str = str[0:l-2]	// chop *.( and ).*
		tokens := strings.Split( str, "," )
		v := 0
		for _, cv := range tokens {
			i := atoi( cv )
			v = (v * 10) + i
		}

		return v
	}

	if len(str) > 10 && str[0:10] == "light-dark" {
		str = strings.Trim( str, " " )
		str = str[10:]
		l := len( str ) - 1
		tokens := strings.Split( str[1:l], "," )
		if len(tokens) < 2 || !invertColours {
			return string2colour( tokens[0] )
		} else {
			return string2colour( tokens[1] )
		}
	}

	if invertColours {
		return 0
	}
	return 0xffffff
}

/*
	Internal function to set the composite value and string based on the rgb.
*/
func (c *Colour) setCs() {
	c.cval = (c.red << 16) + (c.green << 8) + c.blue
	c.str = fmt.Sprintf("0x%02x%02x%02x", c.red, c.green, c.blue)
}

func NewColour(colour string) (c *Colour) {
	c = &Colour{}

	colour = strings.Trim( colour, " " )
	c.SetColour(colour)

	return c
}

/*
	Set the colour represented by the struct. Arg is expected as a string (0xrrggbb,  #rrggbb,
	or the hidous rgb(r,b,b) representation that io.draw shoves into their embedded HTML).
*/
func (c *Colour) SetColour( colour string ) {
	if c == nil {
		return
	}

	c.str = colour
	c.cval = string2colour(c.str)
	cv := c.cval
	c.blue = cv % 256

	cv = cv >> 8
	c.green = cv % 256

	cv = cv >> 8
	c.red = cv % 256
	//fmt.Fprintf( os.Stderr, "%% >>>>> setcolout from %s to %d %d %d\n", colour, c.red, c.green, c.blue )
}

/*
	Return the composite value as an integer.
*/
func (c *Colour) Composite() int64 {
	if c == nil {
		return 0
	}

	return int64(c.cval)
}

/*
	Return the red green and blue values.
*/
func (c *Colour) RGB() (int, int, int) {
	if c == nil {
		return 128, 128, 128
	}

	if c.red < 5 && c.green < 5 && c.blue < 5 {
		return 255, 255, 255
	}
	if c.red > 250 && c.green > 250 && c.blue > 250 {
		return 0, 0, 0
	}

	return c.red, c.green, c.blue
}

/*
	Return the red green and blue values as floating percentages of 255.

	If invert BW only flag is true, then only black and white are inverted.
	When invertColours is true, then all colours are inverted.
*/
func (c *Colour) RgbPct() (float64, float64, float64) {
	if c == nil {
		return 128.0, 128.0, 128.0
	}

	if invertColours {
		if c.red < 5 && c.green < 5 && c.blue < 5 {	// consider this black
			return 1.0, 1.0, 1.0
		}
		if c.red > 250 && c.green > 250 && c.blue > 250 { // consider this white
			return 0.0, 0.0, 0.0
		}

		// black and white (ish) only. Assume all otheer colours will be inverted with light-dark() css crap.
		//return float64( 255 - c.red )/255.0,  float64( 255.0  -c.green )/255.0, float64( 255 - c.blue  )/255.0
	}


	return float64(c.red) / 255.0, float64(c.green) / 255.0, float64(c.blue) / 255.0
}

/*
	The string interface is in the form that Postscript colour command wants
*/
func (c *Colour) String() string {
	if c == nil {
		return "0 0 0"		// black
	}

	rp, rg, rb := c.RgbPct()
	return fmt.Sprintf( "%.2f %.2f %.2f",  rp, rg, rb )
}
