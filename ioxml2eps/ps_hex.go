/*
	Abstract:	A hexigon. The inset value is the amount of space that the
				top and base are "inset" from the far left and right points.

	Date:		11 Apr 2024
	Author:		E. Scott Daniels
*/

package ioxml2eps

import (
	"io"
)

var psHexWritten bool = false
var psHexCode string = `
  % create the path of the hexagon
    % x y width height inset hexagon path
	/hexagon {
		/_hxinset ed
		newpath
		4 2 roll exch _hxinset add exch moveto	% left top edge
		exch dup _hxinset 2 mul sub				% height width base-width
		0 rlineto exch dup          		% width height height
		2 div _hxinset exch         		% width height inset h/2
		rlineto dup 2 div           		% width height h/2 (out to right point)
		_hxinset neg exch           		% width height -inset h/
		rlineto                     		% width height (back to base)
		exch _hxinset 2 mul sub neg 0       % height -dist 0
		rlineto 2 div               		% h/2
		_hxinset  neg exch neg				% -dist -h/2
		rlineto
		closepath
	} def


	% filled hexagon thing; stroking with current colour
	% r g b x y width height tailPos fhexagon
	/fhexagon {
		5 copy hexagon	% dup everything and set the path
		gsave
			8 5 roll
			setrgbcolor	
			fill
		grestore
		hexagon
		stroke
	} def

	% fill only hexagon thing (no stroke)
	% r g b x y width height tailPos fohexagon
	/fohexagon {
		hexagon
		gsave
			setrgbcolor
			fill
		grestore
	} def
`

func psHexWrite( out io.Writer ) {
	if ! psHexWritten {
		psUtilWrite( out )	// ensure we are set up
		out.Write( squishPS( psHexCode, true ) )
		psHexWritten = true
	}
}
