/*
	Abstract:	Postscript to support drawing ellipses.

	Date:		25 April 2022
	Author:		E. Scott Daniels
*/

package ioxml2eps

import (
	"io"
)


var psEllipseWritten bool = false
var psEllipseCode string = `
	% ellipse -- set the elipse path; caller MUST enclose in gsave/restore
	% x y width height; x,y is the centre of the ellipse. When width
	% and height are equal the result is a circle.
	/elipPath {
			4 2 roll translate
			/_h exch def /_w exch def
			1 _h _w div scale
			0 0 _w 2 div 0 360 arc
	} def

	% stroked ellipse
	/ellipse {
		gsave
		elipPath stroke
		grestore
	} def

	% filled and stroked ellipse
	% current colour is the stroke colour, r,g,b on stack is the fill colour
	% x,y is UPPER left
	% r g b x y width height
	/fellipse {
		gsave
			gsave
				4 copy		% need a copy because we use x,y,w,h to fill then to stroke
				elipPath 
				7 4 roll
				setrgbcolor
				fill
			grestore

			elipPath stroke
		grestore
	} def

	% filled only ellipse
	% r g b x y width height
	/foellipse {
		gsave
			elipPath 
			7 4 roll
			setrgbcolor
			fill
		grestore
	} def
`

func psEllipseWrite( out io.Writer ) {
	if ! psEllipseWritten {
		psUtilWrite( out )	// ensure we are set up
		out.Write( squishPS( psEllipseCode, true ) )
		psEllipseWritten = true
	}
}
