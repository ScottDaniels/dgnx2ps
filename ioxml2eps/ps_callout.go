
/*
	Abstract:	a callout box (caption box).

				Poor ascii art representation.
					-----------
 					|         |
					|_____  __|
                          / /
                          |/

				The point of the "tail" is listed in the html style as "position2"
				and is set as a percentage of the width.

				Height is the height of the box; decent is the distance between the
				box and the point of the tail.

	Date:		11 Apr 2024
	Author:		E. Scott Daniels
*/

package ioxml2eps

import (
	"io"
)

var psCallOutWritten bool = false
var psCallOutCode string = `
	% create the path of the callout
	% x y width height lGapX lGapY decX decY rGapX rGapY  callout path
	/callout {
		/_coRGapY ed
		/_coRGapX ed
		/_coDecY ed
		/_coDecX ed
		/_coLGapY ed
		/_coLGapX ed
		/_coHeight ed
		/_coWidth ed
		/_coy ed
		/_cox ed
		newpath
		_cox _coy moveto					% ul corner 
		_coWidth 0 rlineto					% top line
		0 _coHeight neg rlineto				% right side
		_coRGapX _coRGapY lineto			% right bottom
		_coDecX _coDecY lineto				% right side decender
		_coLGapX _coLGapY lineto			% left side decender
		_cox _coy _coHeight sub lineto
		closepath
	} def

	% filled callout thing; stroking with current colour
	% r g b x y width height tailInset tailDepth fcallout
	% r g b x y width height lGapX lGapY decX decY rGapX rGapY  fcallout
	/fcallout {
		10 copy callout	% dup everything except fill colour, and set the path
		gsave
			13 10 roll
			setrgbcolor	
			fill
		grestore
		callout		% reline the path and stroke it
		stroke
	} def

	% fill only callout thing (no stroke)
	% r g b x y width height lGapX lGapY decX decY rGapX rGapY  focallout
	/focallout {
		callout
		gsave
			setrgbcolor
			fill
		grestore
	} def
`

func psCallOutWrite( out io.Writer ) {
	if ! psCallOutWritten {
		psUtilWrite( out )	// ensure we are set up
		out.Write( squishPS( psCallOutCode, true ) )
		psCallOutWritten = true
	}
}
