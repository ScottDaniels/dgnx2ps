
package ioxml2eps

import (
	"fmt"
	"io"
	"math"
	"os"
	"strconv"
	"strings"
)

const (
	deg2rad float64 = 0.0174533
	r360 float64 = 360 * deg2rad
	r180 float64 = 180 * deg2rad
)

/*
	Given an alignment string from a style list, convert to one or our
	alignment constants.
*/
func toAlignment( what string ) Alignment {
	switch strings.ToLower(what) {
		case "right":
			return alignRight

		case "left":
			return alignLeft

		case "center":
			return alignCenter

		case "top":
			return alignTop

		case "bottom":
			return alignBottom

		case "middle":
			return alignMiddle
	}
	
	return unkAlign
}

/*
	Given a position string from a style list, convert to one or our
	position constants.
*/
func toPosition( what string ) Position {
	switch what {
		case "top":
			return posTop

		case "bottom":
			return posBottom

		case "middle":
			return posMiddle

		case "right":
			return posRight

		case "left":
			return posLeft
	}
	
	return unkPos
}

// ------------------- conversions ----------------------------------------------------------------
/*
	Convert string to integer without panicking. In the same sense as the original C functions
	these stop parsing at the first non-digit and don't panic just because the string contains
	trailing units, % signs, decimal points etc.
*/
func atoi( src string)  int {
	var err error

	src = strings.Trim( src, " " )
	if src == "" {
		return 0
	}

	i := 0
	if src[i] == '-' || src[i] == '+' {
		if len( src ) < 2 {
			return 0
		}
		i++
	}

	buf := []byte( strings.ToLower(src) )

	if len( buf ) > 2 && string(buf[0:2]) == "0x" {
		i += 2
		for ; i < len(buf)  &&  ((buf[i] >= '0'  &&  buf[i] <= '9') || (buf[i] >= 'a' && buf[i] <= 'f') ); i++ {}	// find last digit
	} else if src[0] == '0' {
		for ; i < len(buf)  &&  (buf[i] >= '0'  &&  buf[i] <= '7'); i++ {}							// find last octal digit
	} else {
		for ; i < len(buf)  &&  (buf[i] >= '0'  &&  buf[i] <= '9'); i++ {}	// find last base 10 digit
	}

	v := int64(0)
	if i > 0 {
		v, err = strconv.ParseInt( string( buf[0:i] ), 0, 64 )
		if err != nil {
			return 0
		}
	}

	return int( v )
}

/*
	Given a float number of the form [+|-][0-9]*[.[0-9]*] return the float value.
	Like atoi, this doesn't panic and returns 0 if the string passed in doesn't
	represnent a number, or has trailing non-numeic characters (e.g. 4.30%).
*/
func atof( src string)  float64 {
	var err error

	if src == "" {
		return 0
	}

	buf := []byte( strings.ToLower(src) )
	if len( buf ) == 0 {
		return 0.0
	}

	i := 0
	if buf[i] == '+' || buf[i] == '-' {
		i++
	}
	for ; i < len(buf) && buf[i] >= '0' && buf[i] <= '9'; i++ {}
	if i < len(buf) {
		if buf[i] == '.' {
			i++
			for ; i < len(buf) && buf[i] >= '0' && buf[i] <= '9'; i++ {}
		}
	}

	v := float64(0)
	if i > 0 {
		v, err = strconv.ParseFloat( string( buf[0:i] ), 0 )
		if err != nil {
			return 0.0
		}
	}

	return v
}

// ------------------------- drawing support --------------------------------------------
/*
	Not to be confused with a layout Point which manages things
	read from the XML as strings. 
*/
type RealPoint struct {
	x	int
	y	int
}

func NewRealPoint( x int, y int ) *RealPoint {
	return &RealPoint {
		x:	x,
		y:	y,
	}
}

/*
	Given a list of real points, return the two points representing the
	min x,y and max x,y pair.
*/
func BoundingBox( pts []*RealPoint ) (minPt *RealPoint, maxPt *RealPoint) {
	if len( pts ) == 0 {
		return nil, nil
	}

	x, y := pts[0].XY()
	minX, minY := x, y
	maxX, maxY := x, y

//fmt.Fprintf( os.Stderr, "%% >>> bb default %d,%d  %d,%d\n", minX, minY, maxX, maxY )
	for i := 1; i < len(pts); i++ {
		x, y = pts[i].XY()
		if maxX < x {
			maxX = x
		}
		if minX > x {
			minX = x
		}
		if maxY < y {
			maxY = y
		}
		if minY > y {
			minY = y
		}
//fmt.Fprintf( os.Stderr, "%% >>> bb update %s  %d,%d  %d,%d\n", pts[i], minX, minY, maxX, maxY )
	}

	minPt = NewRealPoint( minX, minY )
	maxPt = NewRealPoint( maxX, maxY )
	return minPt, maxPt
}

/*

	x1, y1 := pt.XY()
	x2, y2 := pt2.XY()

	smX := x1
	lgX := x2
	if x2 < x1 {
		smX = x2
		lgX = x1
	}

	smY := y1
	lgY := y2
	if y2 < y1 {
		smY = y2
		lgY = y1
	}

	minPt = NewRealPoint( smX, smY )
	maxPt = NewRealPoint( lgX, lgY )

	return minPt, maxPt
}
*/

/*
	Return the x,y coordinate of the point
*/
func (pt *RealPoint) XY() (int, int) {
	if pt == nil {
		return 0, 0
	}

	return pt.x, pt.y
}

/*
	Rotate the point angle degrees round the centre point.
	Returns a new point (old point left intact)
*/
func (pt *RealPoint) Rotate( centre *RealPoint, angle float64 ) *RealPoint {
	if pt == nil {
		return NewRealPoint( 0, 0 )
	}

	cx, cy := centre.XY( )		// get the centre of rotation
	fcx := float64( cx )
	fcy := float64( cy )
	ox, oy := pt.XY()			// original x,y

	if ox == cx && oy == cy {
		return NewRealPoint( ox, oy )	// no rotation if it is the centre
	}

	x := float64( ox - cx )
	y := float64( oy - cy )
	r := math.Sqrt( float64(x * x) + float64(y * y) )

	a := float64( 0.0 )				// compute angle to current point
	ra := angle * deg2rad			// rotaional angle in radians
	if x >= 0 && y >= 0  {			// figure the new angle based on quadrant current  point in relative to center
		a = math.Asin(  y/r )
	} else if x < 0 && y >= 0 {
		a = r180 - math.Asin( y/r )	
	} else if x < 0 && y < 0  {
		a = r180 + math.Asin( math.Abs(y)/r )
	} else if x >= 0 && y < 0 {
		a = r360 - math.Asin( math.Abs(y)/r )
	}
			
	a += ra
	if a >= (360.0 * deg2rad) {
		a -= 360.0 * deg2rad
	}
	nx := fcx + (math.Cos( a ) * r)
	ny := fcy + (math.Sin( a ) * r)
	//fmt.Printf( "%% <rotate> [%d] (%.2f, %.2f) r=%.2f %.2f\n\n", i, nx, ny, r, a/deg2rad )

	return NewRealPoint( int(nx), int(ny) )
}

/*
	Given a point, and an anchor point on the same line, compute the coords of
	the point dist units away from the point in the direction of the anchor point.
*/
func (pt *RealPoint) PullBack( anchor *RealPoint, dist int ) *RealPoint {
	ax, ay := anchor.XY()
	bx, by := pt.XY()

	h := math.Sqrt( float64( ((ax-bx) * (ax-bx)) + ((ay-by) * (ay-by)) ) )
	angle := math.Asin( float64(ay-by)/h )
	if angle < 0 {
		angle *= -1
	}
	xcb := math.Cos(angle) * float64(dist)
	ycb := math.Sin(angle) * float64(dist)

	cx := 0
	cy := 0
	if bx > ax {
		cx = bx - int(xcb) 
	} else {
		cx = bx + int(xcb) 
	}
	if by > ay {
		cy = by - int(ycb) 
	} else {
		cy = by + int(ycb) 
	}

	return NewRealPoint( cx, cy )
}

/*
	Given a point, compute a new point that is orthognal to pt at a distance
	of d. The angle assumes that a line runs through the point at
	the indicated angle (degrees).
*/
func (pt *RealPoint) SlideOver( dist int, angle float64 ) *RealPoint {
	if pt == nil {
		return NewRealPoint( 0, 0 )
	}

	if dist == 0 {
		dist = 1
	}

	x, y := pt.XY()
	x +=  dist
	np := NewRealPoint( x, y )
	return np.Rotate( pt, angle + 90.0 )
}


/*
	Point as a string: "x,y"
*/
func (pt *RealPoint) String() string {
	if pt == nil {
		return "%% <nil-pt>"
	}

	return fmt.Sprintf( "%d %d", pt.x, pt.y )
}

/*
	Return the string needed to paint the point as a small square
	centered on the point's coordinates..
*/
func (pt *RealPoint) Paint() string {
	if pt == nil {
		return "%% <nil-pt>"
	}

	return fmt.Sprintf( "%d %d moveto 4 0 rlineto 0 4 rlineto -4 0 rlineto closepath fill", pt.x-2, pt.y-2  )
}


// -------------------------- file support ----------------------------------------------
/*
    Open the named file for reading
*/
func openRead(fname string) (io.Reader, error) {
	file, err := os.OpenFile(fname, os.O_RDONLY, 0000)
	return file, err
}

/*
    Opens the file and returns the size in bytes if the open
    is successful.
*/
func openReadSize( fname string ) ( io.Reader, int64, error ) {
    f, e := openRead( fname )
    len := int64( -1 )
    if e == nil {
        len, e = f.(*os.File).Seek( 0, 2 )
        if e == nil {
            f.(*os.File).Seek( 0, 0 )               // set to the beginning
        }
    }

    return f, len, e
}

/*
	The sad part about xml and jscript is that we seem to need to read
	the whole bloody thing into memory before we can process it. Sigh.
	This function does that and we hope that we don't push the box into
	a never ending paging death spirle.  We could stat the file and
	refuse to read more than a certain amount....
*/
func readFile( fname string )  ([]byte, error) {
	rf, fsize, err := openReadSize(fname)
    if err != nil {
		fmt.Fprintf( os.Stderr, "read file: open failed: %s: %s", fname, err)
        return nil, err
    }
    defer rf.(*os.File).Close()

	if fsize > 4 * 1024 * 1024 {
		return nil, fmt.Errorf( "cannot read file: too large: %dKiB\n", fsize/1024 )
	}

    rbuf := make([]byte, fsize)
    _, err = rf.Read(rbuf)
    if err != nil {
		fmt.Fprintf( os.Stderr, "file load failed: %s: %s", fname, err)
        return nil, err
    }

	return rbuf, nil
}

