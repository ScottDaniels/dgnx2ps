
package ioxml2eps

/*
	Text style constants. These come off of the xml, so they are
	strings and not ints.
*/
const (
	normalText	string = "0"
	boldText	string = "1"
	italText	string = "2"
)

type Position int
const (
	posTop		Position = iota
	posBottom
	posMiddle
	posRight
	posLeft
	posCenter
	unkPos
)

type Alignment int
const (
	alignRight	Alignment = iota
	alignCenter
	alignLeft
	alignTop
	alignBottom
	alignMiddle
	unkAlign
)

type arrowKind int
const (
	arrowNone 		arrowKind = iota
	arrowRegular
	arrowOval
	arrowHalf		// rt triangle "half" arrow
)

/*
	Drawing objets (mxCell) generally don't have a "type". Where you
	might expect to find a kind="circle" or type="rect" attr in the style
	string, none exists. So, a convoluted set of rules are needed to
	"map" a style to an object kind these rules are based on the syle
	string and implemented in style, but the kind of thing constants
	and API make sense to be here.

	Line things, and maybe others, need to be able to find other drawing
	things in order to compute real src and target points when they are 
	"attached".  To that end, the object supports references for src and 
	target things when they are defined.
*/
type DthingKind string
const (
	dtUnknown		DthingKind = "wtf?"
	dtCallOut		DthingKind = "callout"	// commic bubble
	dtCircle		DthingKind = "circle"
	dtCloud			DthingKind = "cloud"
	dtCylinder		DthingKind = "cylinder"
	dtDashedLine	DthingKind = "dashedLine"
	dtFlexArrow		DthingKind = "flexFrrow"
	dtHexagon		DthingKind = "hexagon"
	dtLine			DthingKind = "line"
	dtOval			DthingKind = "oval"
	dtParGram		DthingKind = "parallelogram"
	dtRect			DthingKind = "rect"
	dtReport		DthingKind = "report"
	dtRhombus		DthingKind = "rhomb"
	dtRoundRect		DthingKind = "roundRect"
	dtStep			DthingKind = "step"	// cheveron looking box
	dtStorage		DthingKind = "storage"
	dtTangle		DthingKind = "tangle"	// triangle
	dtText			DthingKind = "text"
	dtTrap			DthingKind = "trap"
)

/*
	Attached entry/exit types.
*/
type attKind string
const (
	attEntry	attKind = "entry"
	attExit		attKind = "exit"
)

/*
	The style settings are keyed off of the names in the XML. To insulate
	the code as best we can from changes to the XML names, the style key
	name constants should be used to interact with the settings map. There
	are also synonyms because some of the embedded HTML has different
	strings associated with the same style setting.
*/
type StyleKey string
const (
	skAlign StyleKey = "align"
	skBase StyleKey = "base"
	skCallOut StyleKey = "callout"			// cartoon text bubble thing
	skCurved StyleKey = "curved"
	skDashed StyleKey = "dashed"
	skDashPat	StyleKey = "dashPattern"	// likely <line-len><blank><space-len>
	skEllipse StyleKey = "ellipse"
	skEndArrow StyleKey = "endArrow" 		// none, classic, classicThing, async, oval
	skEndFill StyleKey = "endFill"			// seems to be 1 or 0
	skEntryX StyleKey = "entryX"			// x,y for "target" attachement as a bleeding percentage of the target's dimensions
	skEntryY StyleKey = "entryY"
	skExitX StyleKey = "exitX"				// x,y for "source" attachement as a bleeding percentage of the source's dimensions
	skExitY StyleKey = "exitY"
	skFillColour StyleKey = "fillColor"
	skFont StyleKey = "font"
	skFontColour StyleKey = "fontColor"
	skFontSize StyleKey = "fontSize"
	skFontStyle StyleKey = "fontStyle"		// 0 == normal, 1 == bold 2 == ital
	skLabelPos StyleKey = "labelPosition"
	skLineColour StyleKey = "strokeColor"
	skLineWidth StyleKey = "strokeWidth"
	skPosition StyleKey = "position"		// various percentage positions relative to some unnown value
	skPosition2 StyleKey = "position2"
	skRhombus StyleKey = "rhombus"
	skRotate StyleKey = "rotation"
	skRounded StyleKey = "rounded"
	skShape StyleKey = "shape"
	skStep StyleKey = "step"			// right pointing chevron (a process step)
	skSize StyleKey = "size"
	skSrcThing StyleKey = "source"
	skStartArrow StyleKey = "startArrow" // none, classic
	skStartFill StyleKey = "startFill"
	skTargetThing StyleKey = "target"
	skTangle StyleKey = "triangle"
	skVertAlign StyleKey = "verticalAlign"
	skLabPos StyleKey = "labelPosition"	// top topLeft bottom...
	skText StyleKey = "text"
	skTriangle StyleKey = "triangle"
	skVertLabPos StyleKey = "verticalLabelPosition"	// top topLeft bottom...
)
