/*
	Abstract:	unit tests for the value functions.
	Date:		31 March 2022
	Author: 	E. Scott Daniels
*/

package ioxml2eps

import (
	"fmt"
	"os"
	"testing"
)

func TestDecode( t *testing.T ) {
	
	//goop := "&lt;div&gt;&lt;b style=&quot;font-size: 16px&quot;&gt;Magic&lt;/b&gt;&lt;/div&gt;&lt;span style=&quot;font-size: 16px&quot;&gt;&lt;div&gt;&lt;b&gt;Circle&lt;/b&gt;&lt;/div&gt;&lt;/span&gt;" 

	//goop  := "&lt;div&gt;&lt;span style=&quot;font-size: 16px&quot;&gt;&lt;b&gt;Filled Magic&lt;br /&gt;Circle&lt;/b&gt;&lt;/span&gt;&lt;/div&gt;" 
	//goop :=  "This is a square&lt;br&gt;thought he&#39;s a&lt;br&gt;pretty cool square and what happens if we run over?"

	goop := "&lt;div style=&quot;text-align: left&quot;&gt;&lt;b style=&quot;color: rgb(0 , 0 , 0)&quot;&gt;This is a square-ish rectangle&lt;/b&gt;&lt;/div&gt;&lt;div style=&quot;text-align: left&quot;&gt;&lt;span style=&quot;color: rgb(0 , 0 , 0)&quot;&gt;thought he&#39;s a&lt;/span&gt;&lt;/div&gt;pretty cool square and what happens if we run over?"

	soup := unescapeHTML( goop )
	fmt.Fprintf( os.Stderr, "%s\n", soup )

	emptyStyle := &Style{}
	value2Eps( os.Stderr, soup, emptyStyle, 100, 100 )
}
