
/*
	Abstract:	The style struct manages the style of the element.

				The blody XML style strings have crap like "foobar;" Are we to assume
				that this really means "foobar=true"?  We do, and that will probably
				screw us in the long run.

				The set of style info is kept as a map. This means that if the style
				key names in the XML change we must change code, but it makes the
				overall maintenance of the style easier. The map should be accessed
				with constant values whch translate to the XML key names to prevent
				messy maintenance if they should change.

				All values are kept as strings and should be translated when used
				if different formats (e.g. colours) are needed.

	Date:		13 March 2022
	Author:		E. Scott Daniels
*/

package ioxml2eps
import (
	"fmt"
	"os"
	"strings"
)

var (
	synonyms	map[string]string
)

/*
	Create the synonym map. The values in this map must match the strings listed
	in the StyleKey constants. When parsing a style string, the synonym map is
	searched for the key and translated to a common one if there. Sadly, the
	draw.io developers weren't consistant between the style strings used in their
	XML and those used in the embedded HTML. Grrrrr.	
*/
func init() {
	synonyms = make( map[string]string, 64 )

	synonyms["font-size"] = string( skFontSize )
	synonyms["font_size"] = string( skFontSize )
	synonyms["fontSize"] = string( skFontSize )
	synonyms["fontsize"] = string( skFontSize )

	synonyms["fontstyle"] = string( skFontStyle )
	synonyms["fontStyle"] = string( skFontStyle )
	synonyms["font-style"] = string( skFontStyle )
	synonyms["font_style"] = string( skFontStyle )

	synonyms["fontfamily"] = string( skFont)
	synonyms["fontFamily"] = string( skFont)
	synonyms["font-family"] = string( skFont)
	synonyms["font_family"] = string( skFont)

	synonyms["fontcolor"] = string( skFontColour )
	synonyms["fontColor"] = string( skFontColour )
	synonyms["font-color"] = string( skFontColour )
	synonyms["font_color"] = string( skFontColour )
	synonyms["color"] = string( skFontColour )

	synonyms["fillcolor"] = string( skFillColour )
	synonyms["fillColor"] = string( skFillColour )
	synonyms["fill_color"] = string( skFillColour )
	synonyms["fill-color"] = string( skFillColour )
	synonyms["bg-color"] = string( skFillColour )
	synonyms["bg_color"] = string( skFillColour )

	synonyms["text-align"] = string( skAlign )	// seen
	synonyms["text_align"] = string( skAlign )	// expect
	synonyms["textAlign"] = string( skAlign )

	synonyms["size"] = string( skSize )
	synonyms["Size"] = string( skSize )
}

type Style struct {
	settings	map[StyleKey]string
}

// -----------------------------------------------------------------------------------------------

/*
	Allows a style to be updated with a key/value pair after it has
	been created by parsing a string.  This is needed for things like
	adding font selection via means other than the style string in either
	the XML or embedded HTML.
*/
func (s *Style) Add( sKey StyleKey, value string ) {
	if s == nil {
		return
	}

	s.settings[sKey] = value
}

/*
	Return true if the key is set in the map.
*/
func (s *Style) IsSet( sk StyleKey ) bool {
	_, state := s.settings[sk]
	return state
}

/*
	Return true if the key is set in the map and has the given value.
*/
func (s *Style) IsSetWith( sk StyleKey, val string ) bool {
	sVal, state := s.settings[sk]
	if state {
		return sVal == val
	}

	return false
}

/*
	Look at the style for the thing and determine what kind
	of a thing it is. Returns a dThing constant.

	Bloody thing can't set a shape= for everything. In some cases there
	is a single token description (e.g. ellipse) with shape= set 
	sometimes, but not always. Lots of edge cases.
*/
func (s *Style) DtKind() DthingKind {
	if s.IsSet( skEndArrow ) || s.IsSet( skStartArrow ){	// lines have an end arrow type
		return dtLine
	}

	shape, hasShape := s.settings[skShape]	// must get for edge case like cloud, so get early

	if s.IsSet( skRhombus ) {	// because shape=xxx isn't good enough for everything; sigh
		return dtRhombus
	}

	if s.IsSet( skEllipse ) {	// bloody thing sets elipse with shape cloud; wtf?
		if hasShape && shape == "cloud" {
			return dtCloud
		}
		return dtOval // cannot tell circle or elipse here; no access to geometry, so everything else is oval
	}

	if s.IsSet( skText ) {
		return dtText
	}

	if s.IsSet( skTangle ) {
		return dtTangle
	}

	if hasShape {
		switch shape {
			case "cylinder", "cylinder3":	// don't grok the meaning of the 3
				return dtCylinder

			case "callout":			// a cheveron looking thing pointing right
				return dtCallOut

			case "dataStorage":
				return dtStorage

			case "document":		// a report/output
				return dtReport

			case "flexArrow":		// hollow wide arrow
				return dtFlexArrow

			case "hexagon":			// a cheveron looking thing pointing right
				return dtHexagon

			case "parallelogram":
				return dtParGram

			case "step":			// a cheveron looking thing pointing right
				return dtStep

			case "trapezoid":			// a cheveron looking thing pointing right
				return dtTrap


			default: 
				//return dtUnknown
				return dtRect
		}
	}

	// if it didn't match above, we treat it as a rectangle
	val, isThere := s.settings[skRounded]
	if isThere && val == "1" {
		return dtRoundRect
	}

	return dtRect		// everything else is a rectangle
}

/*
	Convert the given string to an arrow kind. If the string
	is not recognised, we default to a regular arrow.
*/
func str2ArrowKind( str string ) arrowKind {
	switch str {
		case "open",	// stupid looking, kindergarten, arrow; refine it!
			"classic":
			return arrowRegular

		case "oval":
			return arrowOval

		case "async":
			return arrowHalf

		case "none":
			return arrowNone
	}

	return arrowRegular
}

/*
	Dig out the arrow type for both ends and return. If the
	type is not defined (none) or is not recognised, arrowNone
	is returned.
*/
func (s *Style) ArrowKind() (srcKind arrowKind, targetKind arrowKind) {
	if s == nil || s.DtKind() != dtLine {
		return arrowNone, arrowNone
	}
	
	targetKind = arrowNone
	srcKind = arrowNone

	kind, there := s.settings[skEndArrow] 	// target	
	if there {
		targetKind = str2ArrowKind( kind )
	}
//fmt.Fprintf( os.Stderr, "%% >>>>> endarrow kind=%s %d\n", kind, targetKind )

	kind, there = s.settings[skStartArrow] 	// source
	if there {
		srcKind = str2ArrowKind( kind )
	}

	return srcKind, targetKind
}

/*
	A line may have an arrow at one or both ends. This method
	returns atSrc true if there is an arrow at the point which
	is marked as the start, and similarly if an arrow is applied
	to the point marked as the target.

	The IO.draw XML seems to treat EndArrow as the source and
	StartArrow as the target.
*/
func (s *Style) ArrowLoc() (atSrc, atTarget bool) {
	val, hasVal := s.settings[skEndArrow]
	if hasVal && val != "none"  {
		atTarget = true
	}

	val, hasVal = s.settings[skStartArrow]
	if hasVal  && val != "none" {
		atSrc = true
	}

	//fmt.Fprintf( os.Stderr, "%% >>>> startArrow: %v endArrow: %v\n", atSrc, atTarget )
	return atSrc, atTarget
}

/*
	Look up the raw string in the synonym table and if we find it
	return the normalised key. This allows fontSize, font-size and
	font_size all to map to our common key. If the raw string passed in
	isn't in the hash, just return it as is.
*/
func string2Key( rawStr string ) StyleKey {
	nKey, isThere := synonyms[rawStr]
	if isThere {
		return StyleKey( nKey )
	}
	return StyleKey( rawStr )
}

/*
	Convert the font name and style from XML to something useful.
*/
func cvtFNameStyle( xName string, style string ) string {
	rv := "Helvetica"

	switch xName {
		case "Courier New", "courier", "Courier":
			rv = "Courier"

		case "Times Roman", "Times New Roman", "Times":
			rv = "Times-Roman"
	}

	switch style {
		case normalText:	// no action

		case boldText:
			rv += "-Bold"

		case italText:
			rv += "-Oblique"
		
	}

	return rv
}

/*
	Given a style string parse it into a struct.
	We assume buf is a collection of key=val pairs which are separated with
	semicolons. We also assume that these may be split on separate lines
	by a single newline character. We only set defaults if the flag is true
	so that the HTML styles can "stack".

	In order to parse both the draw.io style of key=value and the html style
	of 'key: value' with a single function, we accep the kvSep string. Once
	again we are left to ponder questions like: if draw.io wants to mix in 
	HTML why can't they match that style form?
*/
func parseStyle( buf string, setDefaults bool, kvSep string ) *Style {
	s := &Style{
		settings:	make(map[StyleKey]string, 17 ),
	}

	if setDefaults {
		s.settings[skFontSize] =  "10"	// set in the defaults for things we care about
		s.settings[skFontStyle] =  normalText
		s.settings[skFont] =  "Helvetica"
		s.settings[skFontColour] =  "#000000"
		s.settings[skFillColour] =  "#ffffff"
		s.settings[skLineColour] =  "#000000"
	}

	recs := strings.Split( buf, "\n" )
	//fmt.Fprintf( os.Stderr, "%%  >>>  found %d recs\n", len(recs))
	for r := range recs {
		//fmt.Fprintf( os.Stderr, "%%  >>>> parsing record: '%s'\n", recs[r] )
		tokens := strings.Split( recs[r], ";" )
		//fmt.Fprintf( os.Stderr, "%%  >>>  found %d token(s) in rec %d: %v\n", len(tokens), r, tokens)
		for _, tok := range tokens {
			//fmt.Fprintf( os.Stderr, "%%  >>>  processing %s\n", tok )
			tok = strings.Trim( tok, " \t" )
			if tok == "" {	// nil token or last k=v had trailing ;
				continue
			}
			//fmt.Fprintf( os.Stderr, "%%  >>>  processing %s\n", tok )
			kv := strings.SplitN( tok, kvSep, 2 )
			//fmt.Fprintf( os.Stderr, "%%  >>>  kv = %s = %s\n", kv[0], kv[1] )
			key := string2Key( string(StyleKey( kv[0] )) )	// normalise varying style key names
			switch len( kv ) {
				case 0:
					// ignore -- clearly this is an empty field

				case 1:
					s.settings[key] = "true"

				default:
					s.settings[key] = kv[1]
			}
		}	
	}

	s.settings[skFont] = cvtFNameStyle( s.settings[skFont], s.settings[skFontStyle] ) 

	return s
}

/*
	Pick out the colurs as postscript values (tripples).
*/
func (s *Style) FontColour() string {
	if s == nil {
		return "0.0 0.0 0.0"
	}

	val, there := s.settings[skFontColour]
	if there {
		return NewColour( val ).String()
	}
	return "0.0 0.0 0.0"
}

func (s *Style) FillColour() string {
	if s == nil {
		return "0.0 0.0 0.0"
	}

	val, there := s.settings[skFillColour]
	if there {
		return NewColour( val ).String()
	}
	return "0.0 0.0 0.0"
}

func (s *Style) LineColour() string {
	if s == nil {
		return "0.0 0.0 0.0"
	}

	val, there := s.settings[skLineColour]
	if there {
		return NewColour( val ).String()
	}
	return "0.0 0.0 0.0"
}

func (s *Style) FontSz() int {
	if s == nil {
		return 10
	}

	val, there := s.settings[skFontSize]
	if there {
		return atoi( val )
	}
	return 10
}

func (s *Style) Align() Alignment {
	if s == nil {
		return alignLeft
	}

	val, there := s.settings[skAlign]
	if there {
		return toAlignment( val )
	}

	return alignLeft
}

func (s *Style) VAlign() Alignment {
	if s == nil {
		return alignTop
	}

	val, there := s.settings[skVertAlign]
	if there {
		return toAlignment( val )
	}

	return alignLeft
}

/*
	Return the horizontal label position.
*/
func (s *Style) LabelPos() Position {
	if s == nil {
		return posTop
	}

	val, there := s.settings[skLabelPos]
	if there {
		return toPosition( val )
	}

	return posTop
}

/*
	Return the size. If found in style, it is >= 0 if missing, and
	caller must use its own default, -1 is returned.
*/
func (s *Style) Size() int {
	if s != nil {
		val, there := s.settings[skSize]
		if there {
			return atoi( val )
		}
	}

	return  -1
}

/*
	Return the vertical label position.
*/
func (s *Style) VertLabelPos() Position {
	if s == nil {
		return posCenter
	}

	val, there := s.settings[skLabelPos]
	if there {
		return toPosition( val )
	}

	return posCenter
}

/*
	Return the rotation amount (degrees)
*/
func (s *Style) Rotation() int {
	if s == nil {
		return 0
	}

	val, there := s.settings[skRotate]
	if there {
		return atoi( val )
	}

	return 0
}

// ------------------ value functions ---------------------------------------------------
/*
	Given a key, fetch the value. The return bool is false if the key isn't in the style.
*/
func (s *Style) Value( key StyleKey ) ( val string, isThere bool) {
	if s == nil {
		return "", false
	}

	val, isThere = s.settings[key]
	return val, isThere
}

/*
	Returns the value from style, or the default if missing.
*/
func (s *Style) FloatValue( key StyleKey, defVal float32 ) (val float32) {
	val = defVal
	if s == nil {
		return val
	}

	v, isThere := s.settings[key]
	if isThere {
		val = float32( atof( v ) )
	}

	return val
}

/*
	Return the value for the given key as an int or the default value
	if it's not there.
*/
func (s *Style) IntValue( key StyleKey, defVal int ) (val int) {
	val = defVal
	if s == nil {
		return val
	}

	v, isThere := s.settings[key]
	if isThere {
		val = atoi( v )
	}

	return val
}


// --------------------------- debugging ------------------------------------------------
/*
	Dump the map; mostly for debugging
*/
func (s *Style) DumpSettings() {
	if s == nil {
		return
	}

	for k, v := range s.settings {
		fmt.Fprintf( os.Stderr, "%s = '%s'\n", k, v )
	}
}
