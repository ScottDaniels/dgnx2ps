
/*
	Abstract:	Unit test for style stack.

	Date:		1 April 2022 (Just fooling round)
	Author:		E. Scott Daniels
*/

package ioxml2eps

import (
	//"fmt"
	//"os"
	"testing"
)

//func (ss *StyleStack) Clear() {

func TestStyleStack( t *testing.T ) {
	defStyle := parseStyle( "fillColor=#123456", true, "=" )		// override default for fill colour to ensure we don't pick a default later
	ss := NewStyleStack( defStyle )

	failIfTrue( t, ss.Len() != 1, "bad length on new style stack" )
	fColour, isThere := ss.Value( skFontColour )
	failIfFalse( t, isThere, "didn't find font colour in default" )

	sString := "endArrow=none;dashed=1;html=1;rounded=0;"
	s := parseStyle( sString, false, "=" )

	ss.Push( s )
	failIfTrue( t, ss.Len() != 2, "bad length on updated style stack" )

	endArr, eaIsThere := ss.Value( skEndArrow )
	failIfFalse( t, eaIsThere, "didn't find style key just added (endArrow)" )
	failIfFalse( t, endArr == "none", "value for endArrow was not correct: " + endArr )

	_, isThere = ss.Value( skEndFill )
	failIfTrue( t, isThere, "found fstyle key (endFill) that shouldn't be there" )

	sString = "endArrow=some;dashed=1;html=1;rounded=0;font-size=20"	// overlays some values
	s = parseStyle( sString, false, "=" )
	ss.Push( s )

	fColour2, isThere := ss.Value( skFontColour )		// font colour wasn't pusehd, should be unchanged
	if ! failIfFalse( t, isThere, "didn't find font colour in default" ) {	// gheck only if found
		failIfFalse( t, fColour2 == fColour, "colours did not match: " + fColour + " != " + fColour2 )
	}

	if eaIsThere {		// if we found it before
		endArr2, isThere := ss.Value( skEndArrow )		// end arrow should have different value
		if ! failIfFalse( t, isThere, "didn't find style key after second push (endArrow)" ) {
			failIfTrue( t, endArr2 == endArr, "end arrow values match" )
		}
	}
	
	l := ss.Len()
	ss.Pop()
	l2 := ss.Len()
	failIfTrue( t, l == l2, "len did not change after pop" )

	if eaIsThere {		// if we found it before
		endArr2, isThere := ss.Value( skEndArrow )		// end arrow should have the same value after pop
		if ! failIfFalse( t, isThere, "didn't find style key after pop  (endArrow)" ) {
			failIfFalse( t, endArr2 == endArr, "after pop expected end arrow to be the same, it was not: " + endArr2 )
		}
	}

	ss.Clear()
	failIfFalse( t, ss.Len() == 1, "len not 1 after clear" )
}
